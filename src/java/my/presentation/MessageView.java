/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.MessageFacade;
import entities.Message;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author alexa
 */
@Named(value = "MessageView")
@RequestScoped
public class MessageView {
    
    private Message message;

    // Injects the MessageFacade session bean using the @EJB annotation
    @EJB
    private MessageFacade messageFacade;

    /**
     * Creates a new instance of MessageView
     */
    public MessageView() {
        this.message = new Message();
    }
    
    // To recieve message
    public Message getMessage(){
        return message;
    }
   
    // Retrurn the number of messages
    public int getNumberOfMessages(){
        return messageFacade.findAll().size();
    }
    
        // Saves the message and then returns the string "theend"
    public String postMessage(){
       this.messageFacade.create(message);
       return "theend";
    }
    public String returnToPrevPage(){
    return "index";
    }// some coment
    
    
}
